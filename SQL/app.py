import mysql.connector
import config as cfg

mydb = mysql.connector.connect(
    host=cfg.mysql['host'],
    database=cfg.mysql['database'],
    user=cfg.mysql['user'],
    password=cfg.mysql['password'],
)
mycursor = mydb.cursor()
mycursor.execute("USE movie")
# mycursor.execute("SHOW TABLES")
# for x in mycursor:
#     print(x)
# ('Comments',)('Movie',)('Rating',)('Reviewer',)('Viewer',)


def queryBuild(query):
    try:
        mycursor.execute(query)
        myresult = mycursor.fetchall()
        for x in myresult:
            print(x)
        return 'Success'
    except (MySQLdb.Error, MySQLdb.Warning) as e:
        print(e)
        return None
    except (TypeError, e):
        print(e)
        return None
    except (ValueError, e):
        print(e)
    finally:
        mycursor.close()
        print('Close')


# SELECT DISTINT => UNIQUE
SELECT = '''
    SELECT DISTINCT movieID        
    FROM rating
'''

# WHERE => haanas
# <> != to same
WHERE_ = ''' 
    SELECT reviewerID, rName
    FROM Reviewer
    WHERE reviewerID != 'R101' 
    <> reviewerID= 'R102'
'''
# AND OR BETWEEN
AND_OR_BETWEEN = '''
    SELECT year1 
    FROM Movie
    WHERE  year1  BETWEEN 1980 AND 2010

    # SELECT year1 
    # FROM Movie
    # WHERE  year1  NOT BETWEEN 1980 AND 2010
'''

# IN NOT IN =>
# james cameronii nairuulsan kino eswel jamaes cameroniin nairuulagui kinog haruulah
IN_NOT_IN = '''
    SELECT * 
    FROM Movie
    WHERE director NOT IN('james cameron')
'''

# LIKE Pattern matching [hew maygaar haih]
LIKE = ''' 
    SELECT *
    FROM movie 
    WHERE title LIKE '_%vat%'
'''
IS_NULL = '''
    SELECT * 
    FROM rating 
    WHERE ratingdate IS NOT NULL # WHERE ratingdate IS  NULL
'''

# 1980 оноос хойш хийгдсэн киноны мэдээлэлийг оноор нь эрэмблэж харуул
# DESC эхээс нь бага
# ASC багаас нь их
# DEFUALT  => asc => багаас нь их
ORDER_BY = '''
    SELECT * 
    FROM movie 
    WHERE year1  > 1980
    ORDER BY year1 ASC
'''

# AGGREGATE: COUNT, SUM, AVG, MIN,MAX
# Багана: attributes

# COUNT(attributes  || *)   => тухайн баганын мөр бичлэгийн тоог буцаана
# SUM(attributes    || INT) => тухайн тоон өгөгдлүүдийн нийлбэрийг буцаана
# AVG(attributes    || INT) => тухайн тоон өгөгдлүүдийн нийлбэрийг дундаж утгыг буцаана
# MIN(attributes    || INT) => тухайн тоон өгөгдлүүдийн хамгийн бага утгыг буцаана
# MAX(attributes    || INT) => тухайн тоон өгөгдлүүдийн хамгийн их утгыг буцаана

COUNT_SUM_AVG_MIN_MAX = '''
    SELECT 
        MAX(stars) # SELECT AVG(stars), SELECT MIN(stars),SELECT SUM(stars)
    FROM rating
'''
COUNT_SUM_AVG_MIN_MAX_ALL_ONE = '''
    SELECT 
        COUNT(stars), 
        SUM(stars), 
        AVG(stars),
        MIN(stars),
        MAX(stars)
    FROM rating
'''

# GROUP BY :    Тухайн баганын ижилхэн өгөгдлүүдийг бүлэглэг
# AS:           Өмнөх баганын нэрийг ард талын утгаар солих
GROUP_BY_AS = '''
    SELECT 
        reviewerID AS USER, 
        COUNT(*)  AS COUNT, 
        MAX(stars) as MAX , 
        MIN(stars) as MIN, 
        avg(stars) AS AVG
    FROM rating
    GROUP BY Reviewerid
'''


# HAVING:  Тийм юмтай ийм юмтай aggregate доторх WHERE : GROUP BY тай хоршиж ажилладаг
reviewerID = 'reviewerID'
HAVING = '''
    SELECT 
        reviewerID AS ХЭРЭГЛЭГЧ , 
        COUNT(*)  AS УДАА, 
        MAX(stars) as STARS , 
        MIN(stars) as MIN, 
        avg(stars) AS AVG
    FROM 
        rating
    GROUP BY 
        Reviewerid
    HAVING 
        УДАА > 3
'''

SUB_QUERY = '''

'''

JOIN = '''
    SELECT c.reviewerID, c.stars
    FROM
        rating AS c
    INNER JOIN
        movie AS r ON c.movieID = r.movieID
    WHERE ratingDate IS NOT NULL AND director IS NOT NULL
'''
NOT_JOIN_USE_JOIN = '''
    SELECT 
        m.title, re.rName, r.stars, r.ratingDate
    FROM
        rating r,
        movie m,
        reviewer re
    WHERE
        r.movieID = m.movieID AND  RE.REVIEWERID = R.REVIEWERID AND r.ratingDATE IS NOT NULL
'''


print(queryBuild(NOT_JOIN_USE_JOIN))
